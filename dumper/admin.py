from __future__ import unicode_literals
from django.contrib import admin

from .models import Truck, TruckType


class TruckAdmin(admin.ModelAdmin):
    list_display = ['number', 'type','weight']



admin.site.register(Truck, TruckAdmin)
admin.site.register(TruckType)
