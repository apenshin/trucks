#!/usr/bin/python
# -*- coding: utf-8 -*-
from django import forms

from .models import TruckType

CHOICES_TYPE = [('', '---')] + list(TruckType.objects.all().values_list('id', 'name'))


class SearchForm(forms.Form):
    search = forms.ChoiceField(required=False, choices=CHOICES_TYPE)
