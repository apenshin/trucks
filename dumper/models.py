#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.db import models
from django.utils.encoding import python_2_unicode_compatible, smart_text


@python_2_unicode_compatible
class TruckType(models.Model):
    name = models.CharField(max_length=150, verbose_name='Название')
    capacity = models.IntegerField(verbose_name='Максимальная грузоподьемность')

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Truck(models.Model):
    number = models.CharField(max_length=10, verbose_name='Бортовой номер')
    type = models.ForeignKey(TruckType)
    weight = models.FloatField(verbose_name='Текущий вес')

    def __str__(self):
        return self.number
