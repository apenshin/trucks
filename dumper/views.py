from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic import ListView

from dumper.forms import SearchForm
from dumper.models import Truck


class TruckListView(ListView):
    model = Truck

    def get_context_data(self, **kwargs):
        context = super(TruckListView, self).get_context_data(**kwargs)
        search = self.request.GET.get('search')
        if search:
            form = SearchForm(self.request.GET)
        else:
            form = SearchForm()
        context['form'] = form
        return context

    def get_queryset(self):
        search = self.request.GET.get('search')
        query = super(TruckListView, self).get_queryset().select_related('type').extra(
            select={
                'overload': 'weight-dumper_trucktype.capacity',
            }
        )
        if search:
            object_list = query.filter(type=search)
        else:
            object_list = query.all()
        return object_list
